package com.template.microservice_1.controller;

import com.template.microservice_1.dto.AddDto;
import com.template.microservice_1.dto.DivideDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.NestedServletException;

import javax.websocket.server.PathParam;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequestMapping(path = "/api")
@RestController
public class ControllerTemplate {

    @GetMapping(path = "/{a}/divide", produces = MediaType.APPLICATION_JSON_VALUE)
    public DivideDto divideMethod(
            @PathVariable Double a,
            @RequestParam Double b
    ) {
        return DivideDto.builder()
                .problem(String.format("%s / %s", a, b))
                .result(String.valueOf(a/b))
                .build();
    }

    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public AddDto addMethod(@RequestBody AddDto req) {
        return AddDto.builder()
                .problem(String.format("%s + %s", req.getA(), req.getB()))
                .result(String.valueOf(req.getA() + req.getB()))
                .build();
    }
}
