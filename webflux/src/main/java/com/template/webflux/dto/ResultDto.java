package com.template.webflux.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResultDto {
    private Double divide;
    private Double add;
}
