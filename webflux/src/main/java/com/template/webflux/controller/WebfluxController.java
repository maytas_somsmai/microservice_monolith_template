package com.template.webflux.controller;

import com.template.webflux.dto.DivideDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/webflux")
public class WebfluxController {

    private WebClient webClient = WebClient.create("http://localhost:8080");
    private static String DIVIDE_API = "/api/{a}/divide",
            ADD_API = "/api/add";

    @GetMapping(path = "/cal", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<DivideDto> cal(
            @RequestParam Double a,
            @RequestParam Double b
    ) {
        return webClient.get().uri(uriBuilder ->
                uriBuilder.scheme("http").host("localhost").port(8080)
                        .path(DIVIDE_API)
                        .queryParam("b", b)
                        .build(a)
        ).retrieve()
        .bodyToMono(DivideDto.class);
    }

}
